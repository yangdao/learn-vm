#[warn(unreachable_code)]
#[warn(dead_code)]
#[warn(unused_must_use)]
#[warn(unused_assignments)]
pub mod types;
pub mod lex;
pub mod errors;
pub mod lex_string;
pub mod layout;
pub mod positions;
pub mod utils;
pub mod traversals;
mod hime_parser;
mod hime_lex;
mod test;
mod my_grammar;
#[macro_use]
extern crate lazy_static;
#[macro_use] 
extern crate lalrpop_util;

pub use hime_lex::{HimeLex};

lalrpop_mod!(pub grammar);