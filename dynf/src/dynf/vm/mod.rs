pub mod vm;
pub mod frame;
pub mod value;
mod scope;

pub use vm::{VM};
pub use frame::Frame;
pub use value::{DObject,DValue,DObjectRef};
pub use scope::Scope;