use super::pos::{Location,Line,Column,BytePos};
use super::str_suffix::{Iter,StrSuffix};

pub struct CharLocations<'a> {
    pub location: Location,
    pub chars: Iter<'a>,
}

impl<'a> CharLocations<'a> {
    pub fn new(input: &'a str) -> CharLocations<'a> {
        CharLocations {
            location: Location {
                line: Line::from(0),
                column: Column::from(1),
                absolute: BytePos::from(1),
            },
            chars:StrSuffix::new(input).iter()
        }
    }
}

impl<'a> Iterator for CharLocations<'a> {
    type Item = (Location,u8);

    fn next(&mut self) -> Option<(Location,u8)> {
        self.chars.next().map(|ch| {
            let location = self.location;
            self.location.shift(ch);
            (location,ch)
        })
    }
}