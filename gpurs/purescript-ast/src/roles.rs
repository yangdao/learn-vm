#[derive(PartialEq,Clone,Debug)]
pub enum Role {
    Nominal,
    Representational,
    Phantom
}