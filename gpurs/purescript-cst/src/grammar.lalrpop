use crate::errors::{ParserErrorType};
use crate::types::{self,SourceToken,Expr,Labeled,Wrapped};
use crate::utils::{self,to_name,to_string};

grammar(errors:&mut Vec<ParserErrorType>);

extern {
    enum SourceToken {
      "(" => SourceToken {value:types::Token::TokLeftParen,..},
      ")" => SourceToken {value:types::Token::TokRightParen,..},
      "{" => SourceToken {value:types::Token::TokLeftBrace,..},
      "}" => SourceToken {value:types::Token::TokRightBrace,..},
      "[" => SourceToken {value:types::Token::TokLeftSquare,..},
      "]" => SourceToken {value:types::Token::TokRightSquare,..},
      "\\{" => SourceToken {value:types::Token::TokLayoutStart,..},
      "\\}" => SourceToken {value:types::Token::TokLayoutEnd,..},
      "\\;" => SourceToken {value:types::Token::TokLayoutSep,..},
      "<-" => SourceToken {value:types::Token::TokLeftArrow(_),..},
      "->" => SourceToken {value:types::Token::TokRightArrow(_),..},
      "<=" => SourceToken {value:types::Token::TokLeftFatArrow(_),..},
      "=>" => SourceToken {value:types::Token::TokRightFatArrow(_),..},
      ":" => SourceToken {value:types::Token::TokColon,..},
      "::" => SourceToken {value:types::Token::TokDoubleColon(_),..},
      "="  => SourceToken {value:types::Token::TokEquals,..},
      "|"  => SourceToken {value:types::Token::TokPipe,..},
      "`"  => SourceToken {value:types::Token::TokTick,..},
      "."  => SourceToken {value:types::Token::TokDot,..},
      ","  => SourceToken {value:types::Token::TokComma,..},
      "_"  => SourceToken {value:types::Token::TokUnderscore,..},
      "\\" => SourceToken {value:types::Token::TokBackslash,..},
      "-"  => SourceToken {value:types::Token::TokOperatorSub,..},
      "@" => SourceToken {value:types::Token::TokOperatorAt,..},
      "#" => SourceToken {value:types::Token::TokOperatorHash,..},
      "ado" => SourceToken {value:types::Token::TokAdo,..},
      "as" => SourceToken {value:types::Token::TokAs,..},
      "case" => SourceToken {value:types::Token::TokCase,..},
      "class" => SourceToken {value:types::Token::TokClass,..},
      "data" => SourceToken {value:types::Token::TokData,..},
      "derive" => SourceToken {value:types::Token::TokDerive,..},
      "do" => SourceToken {value:types::Token::TokDo,..},
      "else" => SourceToken {value:types::Token::TokElse,..},
      "false" => SourceToken {value:types::Token::TokFalse,..},
      "forall" => SourceToken {value:types::Token::TokForall,..},
      "forallu" => SourceToken {value:types::Token::TokForallU,..},
      "foreign" => SourceToken {value:types::Token::TokForeign,..},
      "hiding" => SourceToken {value:types::Token::TokHiding,..},
      "import" => SourceToken {value:types::Token::TokImport,..},
      "if"    => SourceToken {value:types::Token::TokIf,..},
      "in"    => SourceToken {value:types::Token::TokIn,..},
      "infix" => SourceToken {value:types::Token::TokInfix,..},
      "infixl" => SourceToken {value:types::Token::TokInfixl,..},
      "infixr" => SourceToken {value:types::Token::TokInfixr,..},
      "instance" => SourceToken {value:types::Token::TokInstance,..},
      "kind" => SourceToken {value:types::Token::TokKind,..},
      "let" => SourceToken {value:types::Token::TokLet,..},
      "module" => SourceToken {value:types::Token::TokModule,..},
      "newtype" => SourceToken {value:types::Token::TokNewtype,..},
      "nominal" => SourceToken {value:types::Token::TokNominal,..},
      "phantom" => SourceToken {value:types::Token::TokPhantom,..},
      "of" => SourceToken {value:types::Token::TokOf,..},
      "representational" => SourceToken {value:types::Token::TokRepresentational,..},
      "role" => SourceToken {value:types::Token::TokRole,..},
      "then" => SourceToken {value:types::Token::TokThen,..},
      "true" => SourceToken {value:types::Token::TokTrue,..},
      "type" => SourceToken {value:types::Token::TokType,..},
      "where" => SourceToken {value:types::Token::TokWhere,..},
      "(->)" => SourceToken {value:types::Token::TokSymbolArr(_),..},
      LOWER => SourceToken {value:types::Token::TokLowerName(_),..},
      QUAL_LOWER => SourceToken {value:types::Token::TokQualLowerName(_,_),..},
      "(..)" => SourceToken {value:types::Token::TokSymbolDoubleDot,..},
      SYMBOL => SourceToken {value:types::Token::TokSymbolName(_),..},
      QUAL_SYMBOL => SourceToken {value:types::Token::TokQualSymbolName(_,_),..},
      LIT_HOLE =>SourceToken {value:types::Token::TokHole(_),..},
      LIT_CHAR => SourceToken {value:types::Token::TokChar(_,_),..},
      LIT_STRING =>SourceToken {value: types::Token::TokString(_,_),..},
      LIT_RAW_STRING => SourceToken {value:types::Token::TokRawString(_),..},
      LIT_INT =>SourceToken {value: types::Token::TokInt(_,_),..},
      LIT_NUMBER => SourceToken {value:types::Token::TokNumber(_,_),..},
      OPERATOR =>SourceToken {value: types::Token::TokOperator(_),..},
      QUAL_OPERATOR => SourceToken {value:types::Token::TokQualOperator(_,_),..},
      UPPER =>SourceToken {value: types::Token::TokUpperName(_),..},
      QUAL_UPPER => SourceToken {value:types::Token::TokQualUpperName(_,_),..},
    }
}

hole:types::Name<types::Ident> = {
  LIT_HOLE =>  to_name(|s| types::Ident(s) ,<>).unwrap()   
}

forall:SourceToken = {
  "forall",
  "forallu"
}

string:(SourceToken,String) = {
  LIT_STRING => to_string(<>),
  LIT_RAW_STRING => to_string(<>)
}

char:(SourceToken,char) = {
  LIT_CHAR => utils::to_char(<>)
}

number:(SourceToken,Result<i32,f64>) = {
  LIT_INT => utils::to_number(<>),
  LIT_NUMBER => utils::to_number(<>)
}

int:(SourceToken,i32) = {
  LIT_INT => utils::to_int(<>)
}

boolean:(SourceToken,bool) = {
  "true" => utils::to_boolean(<>),
  "false" => utils::to_boolean(<>)
}

pub type0:types::Type<()> = {
  type1,
  <t1:type1> <t2:"::"> <t3:type0> => types::Type::TypeKinded((),Box::new(t1),t2,Box::new(t3)) 
}

pub type1:types::Type<()> = {
  type2,
  <t1:forall> <t2:(typeVarBinding)+> <t3:"."> <t4:type1> => types::Type::TypeForall((),t1,t2,t3,Box::new(t4))
  
}

pub type2:types::Type<()> = {
  type3,
  <t1:type3> <t2:"->"> <t3:type1> =>  types::Type::TypeArr((),Box::new(t1),t2,Box::new(t3)),
  <t1:type3> <t2:"=>"> <t3:type1> => {
    let (c,errs) = utils::to_constraint(&t1);
    if errs.len() > 0 {
      errors.extend(errs);
    };
    types::Type::TypeConstrained((),Box::new(c),t2,Box::new(t3))
  }
}

pub type3:types::Type<()> = {
  type4,
  <t1:type3> <t2:qualOp> <t3:type4> => {
    types::Type::TypeOp((),Box::new(t1),t2,Box::new(t3))
  }
}

type4:types::Type<()> = {
  type5,
  <t1:"#"> <t2:type4> => { 
    types::Type::TypeUnaryRow((),t1,Box::new(t2))
  }
}

pub type5:types::Type<()> = {
  typeAtom,
  <t1:type5> <t2:typeAtom> => {
    types::Type::TypeApp((),Box::new(t1),Box::new(t2))
  },
}

pub typeAtom:types::Type<()> = {
  "_" =>  types::Type::TypeWildcard((),<>) ,
  ident =>  types::Type::TypeVar((),<>) ,
  qualProperName => types::Type::TypeConstructor((),<>) ,
  qualSymbol => types::Type::TypeOpName((),<>),
  string =>  types::Type::TypeString((),<>.0,<>.1) ,
  hole => types::Type::TypeHole((),<>) ,
  "(->)" =>  types::Type::TypeArrName((),<>) ,
  <t1:"{"> <t2:row?> <t3:"}"> => {
    let row = t2.unwrap_or(  types::Row {labels:None,tail:None } );
    let wrap = types::Wrapped {open:t1,close:t3,value:row };
    types::Type::TypeRecord((),wrap)
  },
  <t1:"("> <t2:row?> <t3:")"> => {
    let row = t2.unwrap_or(  types::Row {labels:None,tail:None } );
    let wrap = types::Wrapped {open:t1,close:t3,value:row };
    types::Type::TypeRow((),wrap)
   },
   <t1:"("> <t2:type1> <t3:")"> => {
    let wrap = types::Wrapped {open:t1,close:t3,value:Box::new(t2) };
    types::Type::TypeParens((),wrap)
   },
   //<t1:"("> <t2:typeKindedAtom> <t3:"::"> <t4:type0> <t5:")"> => {
   //  let kind = types::Type::TypeKinded((),Box::new(t2),t3,Box::new(t4));
  //   let wrap = types::Wrapped {open:t1,close:t5,value:Box::new(kind) };
  //   types::Type::TypeParens((),wrap)
  // }
}

typeKindedAtom:types::Type<()> = {
  "_" => types::Type::TypeWildcard((),<>) ,
  qualProperName => types::Type::TypeConstructor((),<>) ,
  qualSymbol => types::Type::TypeOpName((),<>),
  hole => types::Type::TypeHole((),<>) ,
  <t1:"{"> <t2:row?> <t3:"}"> => {
    let row = t2.unwrap_or(  types::Row {labels:None,tail:None } );
    let wrap = types::Wrapped {open:t1,close:t3,value:row };
    types::Type::TypeRecord((),wrap)
  },
  <t1:"("> <t2:row?> <t3:")"> => {
    let row = t2.unwrap_or(  types::Row {labels:None,tail:None } );
    let wrap = types::Wrapped {open:t1,close:t3,value:row };
    types::Type::TypeRow((),wrap)
   },
   <t1:"("> <t2:type1> <t3:")"> => {
    let wrap = types::Wrapped {open:t1,close:t3,value:Box::new(t2) };
    types::Type::TypeParens((),wrap)
   },
   <t1:"("> <t2:typeKindedAtom> <t3:"::"> <t4:type0> <t5:")"> => {
     let kind = types::Type::TypeKinded((),Box::new(t2),t3,Box::new(t4));
     let wrap = types::Wrapped {open:t1,close:t5,value:Box::new(kind) };
     types::Type::TypeParens((),wrap)
   }
}


row:types::Row<()> = {
  <t1:"|"> <t2:type0> =>  types::Row {labels:None,tail:Some((t1,Box::new(t2)))} ,
  <t1:sep<rowLabel,",">> =>  types::Row {labels:Some(t1),tail:None } ,
  <t1:sep<rowLabel,",">> <t2:"|"> <t3:type0> => types::Row {labels:Some(t1),tail:Some((t2,Box::new(t3)))} 
}

rowLabel:types::Labeled<types::Label,Box<types::Type<()>> > = {
  <t1:label> <t2:"::"> <t3:type0> => {
    types::Labeled {label:t1,value:Box::new(t3),sep:t2}
  }
}

label:types::Label = {
  LOWER => utils::to_label(&<>) ,
  LIT_STRING => utils::to_label(&<>) ,
  LIT_RAW_STRING => utils::to_label(&<>) ,
  "ado" => utils::to_label(&<>) ,
  "as" => utils::to_label(&<>) ,
  "case" => utils::to_label(&<>) ,
  "class" => utils::to_label(&<>) ,
  "data" => utils::to_label(&<>) ,
  "derive" => utils::to_label(&<>) ,
  "do" => utils::to_label(&<>) ,
  "else" => utils::to_label(&<>) ,
  "false" => utils::to_label(&<>) ,
  "forall" => utils::to_label(&<>) ,
  "foreign" => utils::to_label(&<>) ,
  "hiding" => utils::to_label(&<>) ,
  "import" => utils::to_label(&<>) ,
  "if" => utils::to_label(&<>) ,
  "in" => utils::to_label(&<>) ,
  "infix" => utils::to_label(&<>) ,
  "infixl" => utils::to_label(&<>) ,
  "infixr" => utils::to_label(&<>) ,
  "instance" => utils::to_label(&<>) ,
  "kind" => utils::to_label(&<>) ,
  "let" => utils::to_label(&<>) ,
  "module" => utils::to_label(&<>) ,
  "newtype" => utils::to_label(&<>) ,
  "nominal" => utils::to_label(&<>) ,
  "of" => utils::to_label(&<>) ,
  "phantom" => utils::to_label(&<>) ,
  "representational" => utils::to_label(&<>) ,
  "role" => utils::to_label(&<>) ,
  "then" => utils::to_label(&<>) ,
  "true" => utils::to_label(&<>) ,
  "type" => utils::to_label(&<>) ,
  "where" => utils::to_label(&<>) ,
}


typeVarBinding:types::TypeVarBinding<()> = {
  ident => types::TypeVarBinding::TypeVarName(<>),
  <t1:"("> <id:ident> <t3:"::"> <t4:type0> <t5:")"> => {
    let errs = utils::check_no_wildcards(&t4);
    if errs.len() > 0 {
      errors.extend(errs);
    };
    let label = Labeled {label:id,sep:t3,value:Box::new(t4) };
    let wrap = Wrapped {open:t1,value:label,close:t5};
    types::TypeVarBinding::TypeVarKinded(wrap)
  }
}

ident: types::Name<types::Ident> = {
  LOWER => to_name(|s| types::Ident(s) ,<>).unwrap(),
  "as" => to_name(|s| types::Ident(s) ,<>).unwrap(),
  "hiding" => to_name(|s| types::Ident(s) ,<>).unwrap(),
  "kind" => to_name(|s| types::Ident(s) ,<>).unwrap(),
  "role" => to_name(|s| types::Ident(s) ,<>).unwrap(),
  "nominal" => to_name(|s| types::Ident(s) ,<>).unwrap(),
  "representational" => to_name(|s| types::Ident(s) ,<>).unwrap(),
  "phantom" => to_name(|s| types::Ident(s) ,<>).unwrap(),
}

pub exprAtom:Expr<()> = {
  "_" => Expr::ExprSection((),<>),
  char => Expr::ExprChar((),<>.0,<>.1)
}

qualOp:utils::QualifiedOpName = {
  OPERATOR =>  utils::to_qual_op_name(errors,<>) ,
  "<=" =>  utils::to_qual_op_name(errors,<>) ,
  "-" =>  utils::to_qual_op_name(errors,<>) ,
  "#" =>  utils::to_qual_op_name(errors,<>) ,
  ":" =>  utils::to_qual_op_name(errors,<>) 
}

qualProperName:utils::QualifiedProperName = {
  UPPER =>  utils::to_qual_proper_name(errors,<>) ,
  QUAL_UPPER =>  utils::to_qual_proper_name(errors,<>) ,
}

qualSymbol:utils::QualifiedOpName = {
  SYMBOL =>   utils::to_qual_op_name(errors,<>) ,
  QUAL_SYMBOL =>   utils::to_qual_op_name(errors,<>) ,
  "(..)" =>  utils::to_qual_op_name(errors,<>) ,
}

sep<A,S>:types::Separated<A> = {
  sep1<A,S> => utils::separated(<>)
}

sep1<A,S>:Vec<(SourceToken,A)> = {
  A =>  vec![(utils::placeholder(),<>)] ,
  <t1:sep1<A,S>> <t2:S> <t3:A> => {
    let mut items = t1;
    items.push((t2,t3)); items 
   }
}

manySep<A,S>:Vec<A> = {

}

manySep1<A,S>:Vec<A> = {
  A => vec![<>],
  <t1:manySep1<A,S>> <t2:S> <t3:A> => {
    let mut items = t1;
    items.push((t2,t3)); items 
  } 
}


pub expr:types::Expr<()> = {
  expr1,
  <t1:expr1> <t2:"::"> <t3:type0> => types::Expr::ExprTyped((),Box::new(t1),t2,t3)
}

expr1:types::Expr<()> = {
  expr2,
  <t1:expr1> <t2:qualOp> <t3:expr2> => types::Expr::ExprOp((),Box::new(t1),t2,Box::new(t3))
}

expr2:types::Expr<()> = {
  expr3,
  <t1:expr2> <t2:"`"> <t3:exprBacktick> <t4:"`"> <t5:expr3> => {
    let wrap = Wrapped {open:t2,value:Box::new(t3),close:t4};
    types::Expr::ExprInfix((),Box::new(t1),wrap,Box::new(t5))
  }
}

exprBacktick:types::Expr<()> = {
  expr3,
  <t1:exprBacktick> <t2:qualOp> <t3:expr3> => types::Expr::ExprOp((),Box::new(t1),t2,Box::new(t3))
}

expr3:types::Expr<()> = {
  expr4,
  <t1:"-"> <t2:expr3> => types::Expr::ExprNegate((),t1,Box::new(t2))
}

expr4:types::Expr<()> = {
  expr5,
  <t1:expr4> <t2:expr5> => utils::expr4_2(t1,t2)
}

expr5:types::Expr<()> = {
  expr6,
  <t1:"if"> <t2:expr> <t3:"then"> <t4:expr> <t5:"else"> <t6:expr> => {
    let if_then_else = types::IfThenElse {
      iteif:t1,
      cond:Box::new(t2),
      then:t3,
      itetrue:Box::new(t4),
      iteelse:t5,
      itefalse:Box::new(t6)
    };
    types::Expr::ExprIf((),if_then_else)
  },
  <t1:doBlock> => types::Expr::ExprDo((),t1)
}


//expr5 :: { Expr () }
//  : expr6 { $1 }
//  | 'if' expr 'then' expr 'else' expr { ExprIf () (IfThenElse $1 $2 $3 $4 $5 $6) }
//  | doBlock { ExprDo () $1 }
//  | adoBlock 'in' expr { ExprAdo () $ uncurry AdoBlock $1 $2 $3 }
//  | '\\' many(binderAtom) '->' expr { ExprLambda () (Lambda $1 $2 $3 $4) }
//  | 'let' '\{' manySep(letBinding, '\;') '\}' 'in' expr { ExprLet () (LetIn $1 $3 $5 $6) }
//  | 'case' sep(expr, ',') 'of' '\{' manySep(caseBranch, '\;') '\}' { ExprCase () (CaseOf $1 $2 $3 $5) }
//  -- These special cases handle some idiosynchratic syntax that the current
//  -- parser allows. Technically the parser allows the rhs of a case branch to be
//  -- at any level, but this is ambiguous. We allow it in the case of a singleton
//  -- case, since this is used in the wild.
//  | 'case' sep(expr, ',') 'of' '\{' sep(binder1, ',') '->' '\}' exprWhere
//      { ExprCase () (CaseOf $1 $2 $3 (pure ($5, Unconditional $6 $8))) }
//  | 'case' sep(expr, ',') 'of' '\{' sep(binder1, ',') '\}' guardedCase
//      { ExprCase () (CaseOf $1 $2 $3 (pure ($5, $7))) }

expr6:types::Expr<()> = {
  exprAtom,
}

doBlock:types::DoBlock<()> = {
  <t1:"do"> <t2:"\\{"> <t3:manySep<doStatement,"\\;">> <t4:"\\}"> => {
      types::DoBlock {
        keyword: t1,
        statements:t3
      }
  }
}



doStatement:types::DoStatement<()> = {
  expr => types::DoStatement::DoDiscard(Box::new(<>)),
  <t1:binder> <t2:"<-"> <t3:expr> => {
    types::DoStatement::DoBind(t1,t2,Box::new(t3))
  }
}


binder:types::Binder<()> = {
  binder1,
  <t1:binder> <t2:"::"> <t3:type0> => types::Binder::BinderTyped((),Box::new(t1),t2,t3)
}


binder1:types::Binder<()> = {
  binder2,
  <t1:binder1> <t2:qualOp> <t3:binder2> => types::Binder::BinderOp((),Box::new(t1),t2,Box::new(t3))
}

binder2:types::Binder<()> = {
  <t1:binderAtom+> => utils::to_binder_constructor(<>).unwrap()
}

binderAtom:types::Binder<()> = {
  "_" =>  types::Binder::BinderWildcard((),<>) ,
  ident => types::Binder::BinderVar((),<>),
  <t1:ident> <t2:"@"> <t3:binderAtom> => types::Binder::BinderNamed((),t1,t2,Box::new(t3)),
  qualProperName => types::Binder::BinderConstructor((),<>,vec![]),
  boolean => types::Binder::BinderBoolean((),<>.0,<>.1),
  char => types::Binder::BinderChar((),<>.0,<>.1),
  string => types::Binder::BinderString((),<>.0,<>.1),
  number => types::Binder::BinderNumber((),None,<>.0,<>.1),
  <t1:"-"> <t2:number> => types::Binder::BinderNumber((),Some(t1),t2.0,t2.1),
//  | delim('[', binder, ',', ']') { BinderArray () $1 }
//   | delim('{', recordBinder, ',', '}') { BinderRecord () $1 }
  <t1:"("> <t2:binder> <t3:")"> => types::Binder::BinderParens((),types::Wrapped {open:t1,close:t3,value:Box::new(t2) })
}