use ordered_float::NotNan;

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub enum Expr {
  Num(Number),
  Opt(Box<Expr>,Opt,Box<Expr>)
}

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub enum Number {
    Int(i64),
    Float(NotNan<f64>),
}

impl Number {
  pub fn case_int(&self) -> i64 {
    match self {
      Number::Int (i) =>  *i,
      Number::Float(i) => panic!("error number"),
    }
  }
}

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub enum Opt {
    Add,
    Sub,
    Mul,
    Div,
}