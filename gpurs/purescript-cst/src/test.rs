use crate::lex::Lexer;
use crate::types::{is_eof,print_source_token};
use crate::errors::{ParserErrorType};
use crate::grammar;

#[test]
fn test_layout() {
    use std::fs::DirEntry;
    let dir = std::fs::read_dir("tests/purs/layout").unwrap();
    for (_, finfo) in dir.enumerate() {
        let file: DirEntry = finfo.unwrap();
        match file.path().extension() {
            Some(ext) if ext == "purs" => {
                let code_string = std::fs::read_to_string(file.path()).unwrap();
                let mut lex = Lexer::new(code_string.as_str());
                let tokens = lex.lex();
                let mut out_string = String::default();
                for tok in tokens {
                    let str = print_source_token(&tok.unwrap());
                    out_string.push_str(str.as_str());
                }
                let mut new_path = file.path();
                new_path.set_extension(".out");
                std::fs::write(new_path,out_string.as_str()).unwrap();
            }
            _ => (),
        };
    }
}

#[test]
fn test_parser() {
   
   let mut lex = Lexer::new("aaa::String");
   let mut tokens = lex.lex();
  
   let ptokens = tokens.drain(0..)
                       .map(|e| {e.unwrap()} )
                       .filter(|t| !is_eof(&t.value) ).map(|e| {println!("{}",&e.value);e}  );

   let type_parser = grammar::type0Parser::new();
   let mut errors:Vec<ParserErrorType> = vec![];
   let _evalue = type_parser.parse(&mut errors,ptokens);
   dbg!(&_evalue);
   dbg!(errors);
}
