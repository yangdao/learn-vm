#[macro_use] extern crate lalrpop_util;

lalrpop_mod!(pub calc);
mod str_suffix;
mod token;
mod ast;
mod vm;

fn main() {
    //token::test_token();
    let code_str = "2 * (3 - 2) + (7 + 1)";
    let tk = token::Tokenizer::new(code_str);
    let ast = calc::ExprParser::new().parse(tk).unwrap();

    let mut op_codes:Vec<vm::OpCode> = vec![];
    vm::build_ast(ast,&mut op_codes);

    let mut ctx = vm::Context::new();
    ctx.set_codes(op_codes);
    ctx.exec();
}
