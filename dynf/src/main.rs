#[macro_use]
extern crate quick_error;
#[macro_use] 
extern crate lalrpop_util;
#[warn(uncommon_codepoints)]
mod dynf;
use dynf::token::{Tokenizer};
use dynf::compiler::compile_file;
use dynf::vm::{VM,Frame,Scope};
use std::sync::{Arc};
use std::collections::HashMap;
#[derive(Default)]
struct Data {
    a:u32,
    b:u32,
    c:u32,
    d:u32
}


fn main() {
    /*
    let code_object = compile_file("main.dynf");
    let mut vec:Vec<Frame> = vec![];
    for i in 0..100000 {
        vec.push(Frame::new(Arc::new(Box::new(code_object.clone()) ) ,Scope::default() ));
        println!("count: {:?}",i);
    };*/

    let mut vm = VM::new_thread();
    let code_object = compile_file("main.dynf");
    vm.run_code_obj(Box::new(code_object) );
    //let t = std::thread::Builder::new().stack_size(100_000 * 0xFF); 
    //t.spawn(|| {
    //   
    //}).unwrap();
    //std::thread::sleep_ms(10000);
}

/*
expr:
 let int = 1;
 let float = 3.14;
 //let string = "123";
 //let char = 'a';
 let array = [1,2,3];
 let dict = {a:1111 , b:2222 };
 let func_call = fun(a);
 let attr = dict.a;
 let index = array[0];
 let c = any.a[0](int);
 let c2 = any[0].a[f(123)](int)(446);
 
func:
 let zero_vec = {x:0,y:0,z:0};
 func vec_scale(vec,num) {
     let a = 1
     let b = 2
     if(a > 10) {
        cccc(a,b);
     }
     return {x:vec.x * num,y:vec.y * num,z:vec.z * num};
 }
*/