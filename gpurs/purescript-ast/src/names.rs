#[derive(PartialEq,Clone,Debug)]
pub enum Ident {
    Ident(String),
    GenIdent(Option<String>,i32),
    UnusedIdent
}

#[derive(PartialEq,Clone,Debug)]
pub struct ModuleName(pub String);

#[derive(PartialEq,Clone,Debug)]
pub enum ProperName {
    TypeName(String),
    ConstructorName(String),
    ClassName(String),
    Namespace(String)
}

#[derive(PartialEq,Clone,Debug)]
pub enum OpName {
    ValueOpName(String),
    TypeOpName(String),
    AnyOpName(String)   
}