pub mod token;
pub mod core;
pub mod ast;
pub mod code;
pub mod compiler;
pub mod vm;

lalrpop_mod!(pub grammar,"/dynf/grammar.rs");

use token::Tokenizer;

pub fn parser_program(source:&str) -> ast::Program {
    let tokenizer = Tokenizer::new(source);
    let tt = tokenizer.map(|v|{
        v.map(|e| (e.span.start(),e.value,e.span.end() ) )
    });
    grammar::ProgramParser::new().parse(tt).unwrap()
}