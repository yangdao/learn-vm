pub mod bytecode;

pub use bytecode::{CodeObject,Instruction,Constant,BinaryOperator,Label,UnaryOperator};